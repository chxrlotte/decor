module bot;

import std.string;

// decor imports
import decor.client;
import decor.lowlevel.gateway;
import decor.logging.logger;
import decor.models.models;

class MyClient: Client
{
    override void onReady(PartialUser user, Guild[] guilds, int discordVersion) {
        infoLog("connected with %s#%s", user.username, user.discriminator);
    }

	override void onMessage(Message msg) {
		if (msg.author.bot)
			return;

        assert(msg.content.startsWith("d."), "was not a command!");

        auto command = msg.content[2..$];

        if (command == "ping") {
            this.http.channel.sendMessage(msg.channel_id, "pong!", false);
        }
	}
}