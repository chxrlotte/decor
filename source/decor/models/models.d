module decor.models.models;

public import decor.models.guild;
public import decor.models.channel;
public import decor.models.role;
public import decor.models.user;
public import decor.models.message;
public import decor.models.emoji;

/** Converts from a JSONValue into T */
T toModel(T)(JSONValue jsonValue) {
    auto model = fromJSON!T(jsonValue);
    return model;
}
