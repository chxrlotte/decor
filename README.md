# decor
Modern Discord API wrapper for D lang.

## Features
- Detailed models to represent gateway responses as D `struct`s
- Decoding basic gateway responses (`DISPATCH` packets) eg `MESSAGE_CREATE`, `GUILD_CREATE`
- Interacting with the HTTP API via an abstract wrapper using `DiscordHTTPClient`
- Providing a class to create a basic bot from that abstracts
  all the complex gateway logic away from the user of the API.

## Basic Example
```d
// bot.d
module bot;

import std.string;

import decor.client;
import decor.logging.logger;
import decor.models.models;

class MyClient : Client
{
    override void onReady(PartialUser user, Guild[] guilds, int discordVersion) {
        infoLog("connected with %s#%s", user.username, user.discriminator);
    }

	override void onMessage(Message msg) {
		if (msg.author.bot)
			return;

        assert(msg.content.startsWith("d."), "was not a command!");
        auto command = msg.content[2..$];

        if (command == "ping") {
            this.http.channel.sendMessage(msg.channel_id, "pong!", false);
        }
	}
}
```

```d
// app.d
import bot;
import vibe.d;

void main() {
    auto client = new MyClient();
    client.login("super secret discord token :-)");

    runApplication();
}